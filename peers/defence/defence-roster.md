---
title: Order of Defence
excerpt: Roster of members
---

## Roster by Seniority in the Order

<table>
<tr><td>
Arenvald von Hagenburg
<br>
 (Nordmark)
<div style="text-align:left">
<ul>
<li>
Elevated as one of three Premiers on 2015/06/27
</li>
</ul>
</div>
</td>
<td>
<img src="{{ site.baseurl }}{% link peers/defence/images/MoDBadge.jpg %}" width="200">
</td>
</tr>


<tr><td>
Cernac the Inspired
<br>
 (Insulae Draconis)
<div style="text-align:left">
<ul>
<li>
Elevated as one of three Premiers on 2015/06/27
</li>
</ul>
</div>
</td>
<td>
<img src="{{ site.baseurl }}{% link peers/defence/images/MoDBadge.jpg %}" width="200">
</td>
</tr>

<tr><td>
&AElig;ir&iacute;kr inn H&aacute;rfagri
<br>
(Nordmark)
<div style="text-align:left">
<ul>
<li>
Elevated as one of three Premiers on 2015/06/27
</li>
</ul>
</div>
</td>
<td>
<img src="{{ site.baseurl }}{% link peers/defence/images/MoDBadge.jpg %}" width="200">
</td>
</tr>

<tr><td>
Fard&auml;ng Skvaldre
<br>
 (Nordmark)
<div style="text-align:left">
<ul>
<li>
Elevated on 2016/01/09
</li>
</ul>
</div>
</td>
<td>
<img src="{{ site.baseurl }}{% link peers/defence/images/MoDBadge.jpg %}" width="200">
</td>
</tr>

<tr><td>
Alexandre Lerot d'Avignon
<br>
  (Insulae Draconis)

<div style="text-align:left">
<ul>
<li>
Elevated on 2016/05/28
</li>
<li>
<a href="http://www.aspiringluddite.com/">An Aspiring Luddite</a>
</li>
<li>
Contact: at events or via information on the above-listed site.
</li>
</ul>

</div>
</td>
<td>
<img src="{{ site.baseurl }}{% link peers/defence/images/YuleCropped.jpg %}" width="200">
</td>
</tr>

<tr><td>
Etienne Fevre de Dion
<br>
 (Insulae Draconis)
<div style="text-align:left">
<ul>
<li>
Elevated on 2016/06/04
</li>
</ul>
</div>
</td>
<td>
<img src="{{ site.baseurl }}{% link peers/defence/images/MoDBadge.jpg %}" width="200">
</td>
</tr>

<tr><td>
Anna von Urwald
<br>
 (Aarnimets&auml;)
<div style="text-align:left">
<ul>
<li>
Elevated on 2017/10/21
</li>
</ul>
</div>
</td>
<td>
<img src="{{ site.baseurl }}{% link peers/defence/images/Anna01.jpg %}" width="200">
</td>
</tr>

<tr><td>
Catlin le Mareschale
<br>
 (Insulae Draconis)
<div style="text-align:left">
<ul>
<li>
Elevated on 2018/08/11
</li>
</ul>
</div>
</td>
<td>
<img src="{{ site.baseurl }}{% link peers/defence/images/MoDBadge.jpg %}" width="200">
</td>
</tr>

<tr><td>
P&oacute;l &Oacute; Briain
<br>
 (Klakavirki)
<div style="text-align:left">
<ul>
<li>
Elevated on 2018/12/08
</li>
</ul>
</div>
</td>
<td>
<img src="{{ site.baseurl }}{% link peers/defence/images/MoDBadge.jpg %}" width="200">
</td>
</tr>
</table>
