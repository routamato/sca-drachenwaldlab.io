---
title: The Dragon's Tale
subtitle: Fusce augue sem cubilia at in cep amet
featureimg: images/offices/chronicler/dtbanner.png
featurealt: Dragon's Tale Covers
---
<table>
<tbody>
<tr>
<td style="text-align: center;"><a href="#dt">Submissions</a></td>
<td style="text-align: center;"><a href="#sub">Subscriptions</a></td>
<td style="text-align: center;"><a href="#addr">Change of Address </a></td>
<td style="text-align: center;"><a href="#ads">Advertising</a></td>
<td style="text-align: center;"><a href="#online">Online Version</a></td>
</tr>
</tbody>
</table>
<h3>The Dragon's Tale</h3>
<p>The <strong><em>Dragon's Tale</em></strong> is the official kingdom newsletter of the Kingdom of Drachenwald and is published monthly, sent by post to subscribing members.</p>
<p>The newsletter is the official source for all Kingdom business, such as changes to law, Royal proclamations, communications from the Crown and Kingdom Officers and event announcements. It also contains the official contact list for Kingdom Officers and local seneschals.</p>
<p>In addition to the above, the newsletter also publishes court reports, the results of heraldic submissions and articles on matters of interest to SCA folk.</p>
<h3><a name="sub"></a>Subscriptions</h3>
<p>Subscriptions are only available to SCA members.&nbsp;&nbsp;Membership can be obtained from the <a href="http://www.sca.org/members/" target="_blank">SCA Membership Pages</a> or from the Office of the Registry, SCA Inc., P.O. Box 360789, Milpitas, CA 95035-0789, USA.</p>
<p>The <em><strong>Dragon's Tale</strong></em> is published electronically, and you will receive an email when the newsletters become available.&nbsp; For those receiving paper copies, these are typically posted around the 20th of the month before the month of issue.&nbsp; If you have not received your newsletter by the 1st of the month, contact the Chronicler.&nbsp; Copies destined for APO addresses may take longer.</p>
<h3><a name="addr"></a>Change of Address</h3>
<p>Changes of address for <em><strong>Dragon's Tale</strong></em> subscriptions must be submitted to the Registry by post at the address above, or using the <a href="http://www.sca.org/members/registry.cgi" target="_blank">online form</a>.&nbsp; <span style="text-decoration: underline;">Do not send address changes to the Chronicler</span>.</p>
<h3><a name="dt"></a>Submissions to the Dragon's Tale</h3>
<p>Contributions of articles, poems, quizzes, crosswords, artwork, photographs, etc for publication in the <em><strong>Dragon's Tale</strong></em> are always welcome.&nbsp; If you have an idea for a contribution, please contact the Chronicler to see if it would be appropriate.&nbsp;</p>
<p>Please see our <a href="{{ site.baseurl }}{% link offices/chronicler/guidelines-contributors.md %}" title="Guidelines for Contributors">Guidelines for Contributors</a> for more information about submitting material to the <em><strong>Dragon's Tale</strong></em>.</p>
<p>Contributions of original material (as opposed to event announcements, officer letters etc) must be accompanied by a form giving us permission to publish.&nbsp; Please use one of the versions below:</p>
<ul>
<li><a href="{{ site.baseurl }}{% link offices/chronicler/files/permission.pdf %}">Permission Form (PDF)</a> - print, complete and post, or scan and email to Chronicler</li>
<li><a href="{{ site.baseurl }}{% link offices/chronicler/email-permission-form.md %}">Permission Form (email)</a> - copy to email, fill in blanks and email to Chronicler</li>
</ul>
<h3><a name="ads"></a>Advertising in the Dragon's Tale</h3>
<p>Advertising for events, products and services of interest to SCA members may be published in the <em><strong>Dragon's Tale</strong></em>, subject to space being available.&nbsp; Advertising copy may be submitted in the same formats as other contributions to the <em>Dragon's Tale,</em> plus, PDF. Payment must be made in advance.&nbsp; Rates below are in Pounds Sterling.&nbsp; For payment in other currencies, please contact the Chronicler.</p>
<div>
<table style="border: 1px solid #000000; width: 420px; height: 87px;" border="1" cellspacing="1" cellpadding="1" align="center">
<tbody>
<tr>
<td style="border: 1px solid #000000; text-align: left;"><strong>&nbsp;Advert Size<br /></strong></td>
<td style="border: 1px solid #000000; text-align: center;"><strong>Single Issue <br /></strong></td>
<td style="border: 1px solid #000000; text-align: center;"><strong>Three or More Issues <br /></strong></td>
</tr>
<tr>
<td style="border: 1px solid #000000;">&nbsp;Full Page</td>
<td style="text-align: center;">&nbsp;£30</td>
<td style="text-align: center;">&nbsp;£25 (per issue)</td>
</tr>
<tr>
<td style="border: 1px solid #000000; text-align: left;">&nbsp;Half Page</td>
<td style="text-align: center;">&nbsp;£15</td>
<td style="text-align: center;">&nbsp;£12 (per issue)</td>
</tr>
<tr>
<td style="border: 1px solid #000000;">&nbsp;Quarter Page</td>
<td style="text-align: center;">&nbsp;£10</td>
<td style="text-align: center;">&nbsp;£8 (per issue)</td>
</tr>
</tbody>
</table>
</div>
<p style="text-align: left;">Classified ads are charged at £2 per 100 words</p>
<h3><a name="online"></a>Dragon's Tale Online</h3>
<p>The <em><strong>Dragon's Tale</strong></em>, as a Kingdom Newsletter, is primarily made available online via <a href="https://members.sca.org/apps/#Newsletters">the main SCA website</a>.</p>
