---
title: Making your event official
---
<h3>What is Official?</h3>
<p>This is a slightly confusing term.&nbsp; There is "official" for the purpose of it being an SCA "Society&nbsp; event", and there is "official" for the purpose of being able to conduct Kingdom business.<br /><a name="local"></a></p>
<h4>Society Event</h4>
<p>That is to say, a planned gathering of folks to do SCA stuff - from a Kingdom Coronation to an evening class in nail-binding, to which any member of the populace may come.</p>
<p>For it to be a Society event, it must be (See <a href="http://www.sca.org/docs/pdf/govdocs.pdf" target="_blank">Corpora II A</a>):</p>
<ol>
<li><strong>Sponsored by a branch of the Society</strong><br />This means that a household, guild or other unofficial part of the SCA cannot sponsor an event by themselves. This is a common area of failure for enthusiastic planners. If you are an incipient branch, then your branch can host the event, but it must be sponsored by an existing branch, most usually the branch that is sponsoring you as an incipient branch.</li>
<li><strong>Registered with the Seneschal of the sponsoring branch.</strong> <br />The seneschal must be aware that the event is taking place. Tangible evidence must exist of the seneschal’s approval. In order that the Calendar Manager knows that the event is registered, the <a href="http://www.drachenwald.sca.org/wordpress/kingdom-calendar/calendar-request/">date request form</a> FIXME must be completed by the branch seneschal rather than the event steward.</li>
<li><strong>Publicized to at least the members of that branch.</strong> <br />Corpora does not specify how notice is to be publicized so printed word or internet newsletter would seem to meet the criteria. And Corpora says “at least members of that branch”. This would imply that telling only five or six friends and ignoring other members would not meet the criteria. This is particularly important when conducting a demo. Please remember that not everybody is on FaceBook, so remember to post to your email list first, then your FaceBook page.</li>
<li><strong>Conducted according to Society rules.</strong> <br />If you don’t know the rules, how can you be sure you have followed them? Demos are Society events and the basic criteria for putting on a demo are covered in the SCA Demo Policy. (See <a href="http://www.sca.org/officers/chatelain/demopolicy.html" target="_blank">SCA Demo Policy</a>)</li>
</ol>
<p><a name="formal"></a></p>
<h4>Official for Formal Business</h4>
<p>Formal actions and announcements must take place at an event that has been notified and published, in advance, in the Kingdom Newsletter.&nbsp; Such business includes:</p>
<ul>
<li>Crown and Coronet Tournaments</li>
<li>Coronations and Investitures</li>
<li>Appointment of Kingdom Officers </li>
<li>Presentation of awards and titles</li>
<li>Proclamation of law</li>
<li>Establishment or advancement of branches</li>
</ul>
<p>For an event to be official for business, it must be:</p>
<ul>
<li>Scheduled on the Kingdom Calendar</li>
<li>Published in the Kingdom Newsletter</li>
</ul>
<p>Furthermore, the event announcement published in the Kingdom Newsletter must include, as a minimum, all the required elements, as described in the <a href="{{ site.baseurl }}{% link offices/chronicler/guidelines-event-announcement.md %}">event announcement guidelines</a>.&nbsp; The <a href="http://www.drachenwald.sca.org/wordpress/kingdom-calendar/calendar-request/">Calendar Date Request Form</a> FIXME provides space for the information needed for a bare-bones announcement, but it is better to provide a full announcement as all the details may not be available at the time the date is requested.</p>
<h4>Which Should We Do?</h4>
<p>If your event is a purely local happening, for members of your local branch (and maybe neighbouring branches), and you aren't expecting to conduct any business (other than branch business), then you need only go with the first four criteria listed above, and publicise the event via your local mailing lists, local newsletter, principality calendar, etc.</p>
<p>If you are expecting any of the formal business listed above to take place, then you need to make the event official for that.&nbsp; This does mean you need to get organised at least two months in advance, to make the publication deadlines.&nbsp; See Scheduling Your Event for more details.&nbsp; Even if you aren't expecting official business, publicising your event in the Kingdom Newsletter means that you can attract attendees from outside your region.&nbsp; Plus, it makes the Calendar look nice and busy.</p>
<h3>Why Make It Official?</h3>
<p>Well, in both cases, it means you are covered by SCA insurance.&nbsp; For the latter, it means that you get free publicity in the Dragon's Tale, more people will see your event, and thus, you may well get more people attending, and, it does mean that, should any official business come up, it will be able to happen at your event.</p>
<h3>Some Other Questions About Events</h3>
<p><strong>Do we have to wear garb at our event?</strong> </p>
<p>Corpora states that attendees should make an attempt at pre-17th century garb. However, this need not apply if, say, it is purely a business meeting, an informal class etc. Also, if the event is just, say, a fighter practice followed by a plate of chips, then period garb is not needed (though one would hope that the armour looks period). <br /> <strong><br />Does it have to be an overnight event?</strong> </p>
<p>No, it can be a one day event or even just an afternoon or evening. If you wish to make a dance practice at a regular Sunday meeting an official event – you can do it! Events can be any reasonable length you want – half a day, one day, two days, a weekend, a week, whatever.  The reason for many events being overnight is that Drachenwald covers a wide geographical area, so many participants in event may have to drive for many hours, fly,or catch ferries to get to an event. It is therefore difficult to do this in one day.   <br /><strong><br />Does it have to have a feast? Or even serve any food?</strong> </p>
<p>Not at all. Serving food of any sort is up to the people running the event. You can serve food, you can have a pot luck where everyone shares, people can bring their own. Each event is different, and does what is best for that event.  It is only fair to let people know in advance what to expect in the way of food so they can make alternative arrangements if necessary.</p>
<p><strong>Does it have to be on a weekend?</strong> </p>
<p>Events can be on any day that you can get people to come to them. We usually have events on weekends, but it is not required.</p>
